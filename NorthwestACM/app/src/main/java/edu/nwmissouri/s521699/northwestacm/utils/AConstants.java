package edu.nwmissouri.s521699.northwestacm.utils;

public class AConstants {

    public static String IS_PROGRAMMINGCONTESTS_CHECKED = "isProgrammingContestsChecked";
    public static String IS_WEBDESIGN_CHECKED =  "isWebDesignChecked";
    public static String IS_OPENSOURCE_CHECKED =  "isOpenSourceChecked";
    public static String IS_GAMEDEVELOPMENT_CHECKED =  "isGameDevelopmentChecked";
    public static String IS_DATASCIENCE_CHECKED = "isDataScienceChecked";
    public static String IS_ANDROID_CHECKED =  "isAndroidChecked";
    public static String IS_OTHER_CHECKED =  "isOtherChecked";
    public static String IS_TUORIAL_VIEWED =  "isTutorialViewed";

    public static String EVENT_NAME =  "eventName";
    public static String EVENT_TIME =  "eventTime";
    public static String EVENT_LOCATION =  "eventLocation";
    public static String IS_NOTIFIED =  "isNotified";

    public static String DATABASE_NAME =  "ACM_CLIENT_DB";
    public static int DATABASE_VERSION =  2;

    public static String EVENT_TABLE_NAME =  "events";
    public static String EVENT_COL_ID =  "eventID";
    public static String EVENT_COL_NAME =  "eventName";
    public static String EVENT_COL_DESCRIPTION =  "eventDescription";
    public static String EVENT_COL_LOCATION =  "eventLocation";
    public static String EVENT_COL_TIME =  "eventTime";
    public static String EVENT_COL_DATE =  "eventDate";
    public static String EVENT_COL_SIG_GROUPS =  "sigGroups";

    public static String STR_SEPARATOR = "__,__";


}
