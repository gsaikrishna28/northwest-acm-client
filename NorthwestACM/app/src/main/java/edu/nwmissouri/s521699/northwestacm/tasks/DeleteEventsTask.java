package edu.nwmissouri.s521699.northwestacm.tasks;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Arrays;

import edu.nwmissouri.s521699.northwestacm.database.EventsDBAdapter;
import edu.nwmissouri.s521699.northwestacm.events.EventsList;
import edu.nwmissouri.s521699.northwestacm.receivers.EventBR;
import edu.nwmissouri.s521699.northwestacm.utils.AConstants;

public class DeleteEventsTask extends AsyncTask<Void, Void, Void> {

    private DeleteEventsTaskCallback deleteEventsTaskCallback;
    private Context context;
    private EventsDBAdapter eventsDBAdapter;
    private ArrayList<EventsList.Event> eventsList;


    public void setDeleteEventsTaskCallback(DeleteEventsTaskCallback deleteEventsTaskCallback) {
        this.deleteEventsTaskCallback = deleteEventsTaskCallback;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... params) {

        if (eventsDBAdapter == null) {
            eventsDBAdapter = new EventsDBAdapter(context);
            eventsDBAdapter.open();
        }

        if (eventsList == null) {
            eventsList = new ArrayList<>();
        }

        Cursor eventsCursor = eventsDBAdapter.getAllEvents();
        eventsCursor.moveToFirst();

        if (eventsCursor.getCount() > 0) {
            do {
                int eventID = eventsCursor.getInt(eventsCursor.getColumnIndex(AConstants.EVENT_COL_ID));
                String eventName = eventsCursor.getString(eventsCursor.getColumnIndex(AConstants.EVENT_COL_NAME));
                String eventDescription = eventsCursor.getString(eventsCursor.getColumnIndex(AConstants.EVENT_COL_DESCRIPTION));
                String eventLocation = eventsCursor.getString(eventsCursor.getColumnIndex(AConstants.EVENT_COL_LOCATION));
                String eventDate = eventsCursor.getString(eventsCursor.getColumnIndex(AConstants.EVENT_COL_DATE));
                String eventTime = eventsCursor.getString(eventsCursor.getColumnIndex(AConstants.EVENT_COL_TIME));
                ArrayList<String> sigGroupsList = new ArrayList<>(Arrays.asList(convertStringToArray(eventsCursor.getString(eventsCursor.getColumnIndex(AConstants.EVENT_COL_SIG_GROUPS)))));

                EventsList.Event event = new EventsList.Event(eventID, eventName, eventDescription, eventLocation, eventDate, eventTime, sigGroupsList);
                eventsList.add(event);
            } while (eventsCursor.moveToNext());
        }

        for (final EventsList.Event event : eventsList) {

            ParseQuery<ParseObject> query = ParseQuery.getQuery("Events");
            query.whereEqualTo("eventName", event.getEventName());
            query.whereEqualTo("eventDescription", event.getEventDescription());
            query.whereEqualTo("eventLocation", event.getEventLocation());
            query.whereEqualTo("eventDate", event.getEventDate());
            query.whereEqualTo("eventTime", event.getEventTime());
            query.whereContainedIn("sigGroups", event.getSigGroups());

            query.getFirstInBackground(new GetCallback<ParseObject>() {
                public void done(ParseObject eventObject, ParseException e) {
                    if (eventObject == null) {
                        String[] sigGroupsStringArray = new String[event.getSigGroups().size()];
                        sigGroupsStringArray = event.getSigGroups().toArray(sigGroupsStringArray);

                        eventsDBAdapter.delete(event.getEventName(), event.getEventDescription(), event.getEventLocation(),
                                event.getEventTime(), event.getEventDate(), convertArrayToString(sigGroupsStringArray));

                        deleteEventsTaskCallback.updateEventsList();
                    } else {
                        if (eventObject.get("eventName").equals(event.getEventName()) &&
                                eventObject.get("eventDescription").equals(event.getEventDescription()) &&
                                eventObject.get("eventLocation").equals(event.getEventLocation()) &&
                                eventObject.get("eventDate").equals(event.getEventDate()) &&
                                eventObject.get("eventTime").equals(event.getEventTime())) {

                            if (((ArrayList<String>) eventObject.get("sigGroups")).size() != event.getSigGroups().size()) {
                                String[] sigGroupsStringArray = new String[event.getSigGroups().size()];
                                sigGroupsStringArray = event.getSigGroups().toArray(sigGroupsStringArray);

                                eventsDBAdapter.delete(event.getEventName(), event.getEventDescription(), event.getEventLocation(),
                                        event.getEventTime(), event.getEventDate(), convertArrayToString(sigGroupsStringArray));

                                Intent intent = new Intent(context, EventBR.class);

                                intent.putExtra(AConstants.EVENT_NAME, event.getEventName());
                                intent.putExtra(AConstants.EVENT_TIME, event.getEventTime());
                                intent.putExtra(AConstants.EVENT_LOCATION, event.getEventDate());
                                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, event.getEventName().hashCode() + event.getEventLocation().hashCode()
                                        + event.getEventTime().hashCode() + event.getEventDate().hashCode(), intent, PendingIntent.FLAG_CANCEL_CURRENT);
                                AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                                alarmManager.cancel(pendingIntent);

                                deleteEventsTaskCallback.updateEventsList();
                            }

                        }
                    }
                }
            });
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        deleteEventsTaskCallback.updateEventsList();

        RetrieveEventsTasks retrieveEventsTasks = new RetrieveEventsTasks();
        retrieveEventsTasks.setEventTaskCallback(eventTaskCallback);
        retrieveEventsTasks.setContext(context);
        retrieveEventsTasks.execute();

    }

    RetrieveEventsTasks.EventTaskCallback eventTaskCallback = new RetrieveEventsTasks.EventTaskCallback() {
        @Override
        public void onRetrievedNewEvent() {
            deleteEventsTaskCallback.updateEventsList();
        }

        @Override
        public void onFinishRetrievingEvents() {
            deleteEventsTaskCallback.onFinishRetrievingEventsTask();
        }
    };


    public interface DeleteEventsTaskCallback {
        void updateEventsList();
        void onFinishRetrievingEventsTask();
    }

    public static String[] convertStringToArray(String str) {
        return str.split(AConstants.STR_SEPARATOR);
    }

    public static String convertArrayToString(String[] array) {
        String str = "";
        for (int i = 0; i < array.length; i++) {
            str = str + array[i];
            if (i < array.length - 1) {
                str = str + AConstants.STR_SEPARATOR;
            }
        }
        return str;
    }
}
