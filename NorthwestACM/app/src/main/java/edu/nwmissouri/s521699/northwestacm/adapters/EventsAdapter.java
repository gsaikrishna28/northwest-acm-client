package edu.nwmissouri.s521699.northwestacm.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import edu.nwmissouri.s521699.northwestacm.R;
import edu.nwmissouri.s521699.northwestacm.events.EventsList;

public class EventsAdapter extends ArrayAdapter<EventsList.Event> {


    public EventsAdapter(Context context, int resource, int textViewResourceId, List<EventsList.Event> objects) {

        super(context, resource, textViewResourceId, objects);

    }

    // Returns the view of event list item
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);

        // Getting text view resources
        TextView eventNameTV = (TextView) view.findViewById(R.id.listItemEventNameTV);
        TextView locationTV = (TextView) view.findViewById(R.id.listItemLocationTV);
        TextView dateTV = (TextView) view.findViewById(R.id.listItemDateTV);
        TextView timeTV = (TextView) view.findViewById(R.id.listItemTimeTV);
        TextView initialLetterTV = (TextView) view.findViewById(R.id.initialLetterTV);

        // Setting text view's text
        eventNameTV.setText(getItem(position).getEventName());
        locationTV.setText(getItem(position).getEventLocation());
        dateTV.setText(getItem(position).getEventDate());
        timeTV.setText(getItem(position).getEventTime());
        initialLetterTV.setText(String.format("%s", getItem(position).getEventName().toUpperCase().charAt(0)));

        // Setting the background color based on letter
        char c = getItem(position).getEventName().toUpperCase().charAt(0);
        int initialLetterASCII = (int)c;
        if (initialLetterASCII < 69) {
            initialLetterTV.setBackgroundColor(Color.parseColor("#135995"));
        } else if (initialLetterASCII > 68 && initialLetterASCII < 73) {
            initialLetterTV.setBackgroundColor(Color.parseColor("#E1711F"));
        } else if (initialLetterASCII > 72 && initialLetterASCII < 77) {
            initialLetterTV.setBackgroundColor(Color.parseColor("#FBBC05"));
        } else if (initialLetterASCII > 76 && initialLetterASCII < 81) {
            initialLetterTV.setBackgroundColor(Color.parseColor("#EA4335"));
        } else if (initialLetterASCII > 80 && initialLetterASCII < 85) {
            initialLetterTV.setBackgroundColor(Color.parseColor("#34A953"));
        } else {
            initialLetterTV.setBackgroundColor(Color.parseColor("#C740B6"));
        }

        return view;
    }
}
