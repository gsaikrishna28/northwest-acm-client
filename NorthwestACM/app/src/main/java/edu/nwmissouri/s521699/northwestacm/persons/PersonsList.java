package edu.nwmissouri.s521699.northwestacm.persons;

import java.util.ArrayList;
import java.util.List;

// This class stores contacts list
public class PersonsList {
    public static List<Person> personsList = new ArrayList<>();

    // A plain old java object to store contact information
    public static class Person {
        private String personName;
        private String personRole;
        private String personPhoneNum;
        private String personEmail;

        public Person(String personName, String personRole, String personPhoneNum, String personEmail) {
            this.personName = personName;
            this.personRole = personRole;
            this.personPhoneNum = personPhoneNum;
            this.personEmail = personEmail;
        }

        public String getPersonName() {
            return personName;
        }

        public String getPersonRole() {
            return personRole;
        }

        public String getPersonPhoneNum() {
            return personPhoneNum;
        }

        public String getPersonEmail() {
            return personEmail;
        }
    }
}
