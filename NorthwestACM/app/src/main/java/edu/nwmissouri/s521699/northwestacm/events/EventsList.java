package edu.nwmissouri.s521699.northwestacm.events;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

// Stores list of events
public class EventsList {

    public static List<Event> eventsList = new ArrayList<>();

    // A plain old java object to store an event
    public static class Event {

        private int eventID;
        private String eventName;
        private String eventDescription;
        private String eventLocation;
        private String eventDate;
        private String eventTime;
        private Calendar cal;
        private ArrayList<String> sigGroups;

        public Event(int eventID, String eventName, String eventDescription, String eventLocation, String eventDate, String eventTime, ArrayList<String> sigGroups) {
            this.eventID = eventID;
            this.eventName = eventName;
            this.eventDescription = eventDescription;
            this.eventLocation = eventLocation;
            this.eventDate = eventDate;
            this.eventTime = eventTime;
            this.sigGroups = sigGroups;

            int month = Integer.parseInt((eventDate.split("/"))[0]);
            int day = Integer.parseInt((eventDate.split("/"))[1]);
            int year = Integer.parseInt((eventDate.split("/"))[2]);
            int hour = Integer.parseInt(eventTime.split(":")[0]);
            int minute = Integer.parseInt(eventTime.split(":")[1].split(" ")[0]);

            Calendar cal = Calendar.getInstance();
            cal.set(year, month, day, hour, minute);

            this.cal = cal;
        }

        public Calendar getCal() {
            return cal;
        }

        public void setCal(Calendar cal) {
            this.cal = cal;
        }

        public int getEventID() {
            return eventID;
        }

        public String getEventName() {
            return eventName;
        }

        public String getEventDescription() {
            return eventDescription;
        }

        public String getEventLocation() {
            return eventLocation;
        }

        public String getEventDate() {
            return eventDate;
        }

        public String getEventTime() {
            return eventTime;
        }

        public ArrayList<String> getSigGroups() {
            return sigGroups;
        }
    }
}
