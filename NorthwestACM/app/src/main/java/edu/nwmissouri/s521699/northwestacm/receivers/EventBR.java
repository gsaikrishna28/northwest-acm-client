package edu.nwmissouri.s521699.northwestacm.receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import edu.nwmissouri.s521699.northwestacm.EventDetailsActivity;
import edu.nwmissouri.s521699.northwestacm.R;
import edu.nwmissouri.s521699.northwestacm.utils.AConstants;

// A broadcast receiver to notify user at event time
public class EventBR extends BroadcastReceiver {

    private Context context;
    private Notification notification;
    private String eventName;
    private String eventTime;
    private String eventLocation;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;

        eventName = intent.getStringExtra(AConstants.EVENT_NAME);
        eventTime = intent.getStringExtra(AConstants.EVENT_TIME);
        eventLocation = intent.getStringExtra(AConstants.EVENT_LOCATION);

        createNotification();
        showNotification();
    }

    private void createNotification() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setSmallIcon(R.drawable.notif);
        builder.setContentTitle(eventName);
        builder.setContentText("Time : " + eventTime + ", Location : " + eventLocation);
        builder.setStyle(new NotificationCompat.BigTextStyle().bigText("Time : " + eventTime + ", Location : " + eventLocation));
        builder.setAutoCancel(true);
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);

        Intent intent = new Intent(context, EventDetailsActivity.class);
        intent.putExtra(AConstants.IS_NOTIFIED, true);
        intent.putExtra(AConstants.EVENT_NAME, eventName);
        intent.putExtra(AConstants.EVENT_TIME, eventTime);
        intent.putExtra(AConstants.EVENT_LOCATION, eventLocation);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, eventLocation.hashCode() + eventName.hashCode(), intent, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(pendingIntent);

        notification = builder.build();
    }

    private void showNotification() {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notification.defaults = Notification.DEFAULT_ALL;
        notificationManager.cancel(10);
        notification.flags |= Notification.FLAG_AUTO_CANCEL | Notification.FLAG_ONLY_ALERT_ONCE;
        notificationManager.notify(10, notification);
    }

}
