package edu.nwmissouri.s521699.northwestacm.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.w3c.dom.Text;

import edu.nwmissouri.s521699.northwestacm.R;
import edu.nwmissouri.s521699.northwestacm.adapters.PersonsAdapter;
import edu.nwmissouri.s521699.northwestacm.persons.PersonsList;
import edu.nwmissouri.s521699.northwestacm.tasks.RetrievePersonsTask;

public class HelpFragment extends ListFragment {

    private ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_help, container, false);
        progressBar = (ProgressBar)view.findViewById(R.id.help_progressBar);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        progressBar.setMax(10);
        progressBar.setVisibility(View.VISIBLE);

        // Retrieving Contacts
        RetrievePersonsTask retrievePersonsTask = new RetrievePersonsTask();
        retrievePersonsTask.setPersonsTaskCallback(personsTaskCallback);
        retrievePersonsTask.execute();

        setListAdapter(new PersonsAdapter(getContext(), R.layout.persons_list_item, R.id.listItemPersonNameTV, PersonsList.personsList));
    }

    RetrievePersonsTask.PersonsTaskCallback personsTaskCallback = new RetrievePersonsTask.PersonsTaskCallback() {
        @Override
        // updating contacts list view new contacts
        public void updatePersonsList() {
            setListAdapter(new PersonsAdapter(getContext(), R.layout.persons_list_item, R.id.listItemPersonNameTV, PersonsList.personsList));
        }

        @Override
        public void onFinishRetrievingPersons() {
            progressBar.setVisibility(View.GONE);
        }
    };
}

