package edu.nwmissouri.s521699.northwestacm.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

import edu.nwmissouri.s521699.northwestacm.EventDetailsActivity;
import edu.nwmissouri.s521699.northwestacm.MainActivity;
import edu.nwmissouri.s521699.northwestacm.R;
import edu.nwmissouri.s521699.northwestacm.adapters.EventsAdapter;
import edu.nwmissouri.s521699.northwestacm.database.EventsDBAdapter;
import edu.nwmissouri.s521699.northwestacm.events.EventsList;
import edu.nwmissouri.s521699.northwestacm.tasks.DeleteEventsTask;
import edu.nwmissouri.s521699.northwestacm.utils.AConstants;

public class EventsListFragment extends ListFragment {

    private EventsDBAdapter eventsDBAdapter;
    private ArrayList<EventsList.Event> eventsList;
    private ArrayList<EventsList.Event> preferredEventsList;
    private SharedPreferences sharedPreferences;

    private boolean isProgrammingContestsChecked;
    private boolean isWebDesignChecked;
    private boolean isOpenSourceChecked;
    private boolean isGameDevelopmentChecked;
    private boolean isDataScienceChecked;
    private boolean iSAndroidChecked;

    private boolean isOnCreateCalled;

    private TextView noEventsTV;
    private ProgressBar progressBar;

    public EventsListFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isOnCreateCalled = true;

        if (!isOnline()) {
            Toast.makeText(getContext(), "Connection is offline", Toast.LENGTH_LONG).show();
        }

        eventsList = new ArrayList<>();
        preferredEventsList = new ArrayList<>();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        isProgrammingContestsChecked = sharedPreferences.getBoolean(AConstants.IS_PROGRAMMINGCONTESTS_CHECKED, true);
        isWebDesignChecked = sharedPreferences.getBoolean(AConstants.IS_WEBDESIGN_CHECKED, true);
        isOpenSourceChecked = sharedPreferences.getBoolean(AConstants.IS_OPENSOURCE_CHECKED, true);
        isGameDevelopmentChecked = sharedPreferences.getBoolean(AConstants.IS_GAMEDEVELOPMENT_CHECKED, true);
        isDataScienceChecked = sharedPreferences.getBoolean(AConstants.IS_DATASCIENCE_CHECKED, true);
        iSAndroidChecked = sharedPreferences.getBoolean(AConstants.IS_ANDROID_CHECKED, true);

        // Deleting events which are deleted by admin and Retrieving new events
        DeleteEventsTask deleteEventsTask = new DeleteEventsTask();
        deleteEventsTask.setContext(getContext());
        deleteEventsTask.setDeleteEventsTaskCallback(deleteEventsTaskCallback);
        deleteEventsTask.execute();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        noEventsTV = (TextView) view.findViewById(R.id.noEventsTV);
        noEventsTV.setVisibility(View.INVISIBLE);
        progressBar = (ProgressBar) view.findViewById(R.id.main_progressBar);
        progressBar.setMax(10);
        progressBar.setVisibility(View.VISIBLE);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!isOnCreateCalled) {
            progressBar.setMax(10);
            progressBar.setVisibility(View.VISIBLE);

            // Deleting events which are deleted by admin and Retrieving new events
            DeleteEventsTask deleteEventsTask = new DeleteEventsTask();
            deleteEventsTask.setContext(getContext());
            deleteEventsTask.setDeleteEventsTaskCallback(deleteEventsTaskCallback);
            deleteEventsTask.execute();

        }

        // Filtering events according to user's choice
        if (MainActivity.isFilterActivityDismissed) {
            isProgrammingContestsChecked = sharedPreferences.getBoolean(AConstants.IS_PROGRAMMINGCONTESTS_CHECKED, true);
            isWebDesignChecked = sharedPreferences.getBoolean(AConstants.IS_WEBDESIGN_CHECKED, true);
            isOpenSourceChecked = sharedPreferences.getBoolean(AConstants.IS_OPENSOURCE_CHECKED, true);
            isGameDevelopmentChecked = sharedPreferences.getBoolean(AConstants.IS_GAMEDEVELOPMENT_CHECKED, true);
            isDataScienceChecked = sharedPreferences.getBoolean(AConstants.IS_DATASCIENCE_CHECKED, true);
            iSAndroidChecked = sharedPreferences.getBoolean(AConstants.IS_ANDROID_CHECKED, true);
            MainActivity.isFilterActivityDismissed = false;
            if (eventsList.size() > 0) {

                preferredEventsList.clear();

                for (EventsList.Event event : eventsList) {
                    boolean isPreferredEvent = false;
                    for (String sigGroup : event.getSigGroups()) {
                        switch (sigGroup) {
                            case "P":
                                if (isProgrammingContestsChecked) {
                                    isPreferredEvent = true;
                                    break;
                                }
                                break;
                            case "W":
                                if (isWebDesignChecked) {
                                    isPreferredEvent = true;
                                    break;
                                }
                                break;
                            case "O":
                                if (isOpenSourceChecked) {
                                    isPreferredEvent = true;
                                    break;
                                }
                                break;
                            case "G":
                                if (isGameDevelopmentChecked) {
                                    isPreferredEvent = true;
                                    break;
                                }
                                break;
                            case "D":
                                if (isDataScienceChecked) {
                                    isPreferredEvent = true;
                                    break;
                                }
                                break;
                            case "A":
                                if (iSAndroidChecked) {
                                    isPreferredEvent = true;
                                    break;
                                }
                                break;
                        }
                    }
                    if (isPreferredEvent) {
                        preferredEventsList.add(event);

                        setListAdapter(new EventsAdapter(getContext(), R.layout.events_list_item, R.id.listItemEventNameTV, preferredEventsList));
                    }
                }
            }
            if (preferredEventsList.size() == 0) {
                setListAdapter(null);
                noEventsTV.setVisibility(View.VISIBLE);
            } else {
                noEventsTV.setVisibility(View.INVISIBLE);
            }
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        isOnCreateCalled = false;
    }

    DeleteEventsTask.DeleteEventsTaskCallback deleteEventsTaskCallback = new DeleteEventsTask.DeleteEventsTaskCallback() {
        @Override
        // Updating events list view with new events
        public void updateEventsList() {
            if (eventsDBAdapter == null) {
                eventsDBAdapter = new EventsDBAdapter(getContext());
                eventsDBAdapter.open();
            }

            eventsList.clear();
            preferredEventsList.clear();
            Cursor eventsCursor = eventsDBAdapter.getAllEvents();
            eventsCursor.moveToFirst();

            if (eventsCursor != null && eventsCursor.getCount() > 0) {
                do {
                    boolean isPreferredEvent = false;
                    int eventID = eventsCursor.getInt(eventsCursor.getColumnIndex(AConstants.EVENT_COL_ID));
                    String eventName = eventsCursor.getString(eventsCursor.getColumnIndex(AConstants.EVENT_COL_NAME));
                    String eventDescription = eventsCursor.getString(eventsCursor.getColumnIndex(AConstants.EVENT_COL_DESCRIPTION));
                    String eventLocation = eventsCursor.getString(eventsCursor.getColumnIndex(AConstants.EVENT_COL_LOCATION));
                    String eventDate = eventsCursor.getString(eventsCursor.getColumnIndex(AConstants.EVENT_COL_DATE));
                    String eventTime = eventsCursor.getString(eventsCursor.getColumnIndex(AConstants.EVENT_COL_TIME));
                    ArrayList<String> sigGroupsList = new ArrayList<>(Arrays.asList(convertStringToArray(eventsCursor.getString(eventsCursor.getColumnIndex(AConstants.EVENT_COL_SIG_GROUPS)))));
                    for (String sigGroup : sigGroupsList) {
                        switch (sigGroup) {
                            case "P":
                                if (isProgrammingContestsChecked) {
                                    isPreferredEvent = true;
                                    break;
                                }
                                break;
                            case "W":
                                if (isWebDesignChecked) {
                                    isPreferredEvent = true;
                                    break;
                                }
                                break;
                            case "O":
                                if (isOpenSourceChecked) {
                                    isPreferredEvent = true;
                                    break;
                                }
                                break;
                            case "G":
                                if (isGameDevelopmentChecked) {
                                    isPreferredEvent = true;
                                    break;
                                }
                                break;
                            case "D":
                                if (isDataScienceChecked) {
                                    isPreferredEvent = true;
                                    break;
                                }
                                break;
                            case "A":
                                if (iSAndroidChecked) {
                                    isPreferredEvent = true;
                                    break;
                                }
                                break;
                        }
                    }
                    EventsList.Event event = new EventsList.Event(eventID, eventName, eventDescription, eventLocation, eventDate, eventTime, sigGroupsList);
                    eventsList.add(event);
                    if (isPreferredEvent) {
                        preferredEventsList.add(event);
                    }
                } while (eventsCursor.moveToNext());
            }

            EventsAdapter eventsAdapter = new EventsAdapter(getContext(), R.layout.events_list_item, R.id.listItemEventNameTV, preferredEventsList);
            setListAdapter(eventsAdapter);
            if (preferredEventsList.size() == 0) {
                noEventsTV.setVisibility(View.VISIBLE);
            } else {
                noEventsTV.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onFinishRetrievingEventsTask() {
            progressBar.setVisibility(View.GONE);
        }
    };

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        // Displaying detailed information of selected event
        Intent detailIntent = new Intent(getContext(), EventDetailsActivity.class);
        detailIntent.putExtra("EVENT_NAME", preferredEventsList.get(position).getEventName());
        detailIntent.putExtra("EVENT_DATE", preferredEventsList.get(position).getEventDate());
        detailIntent.putExtra("EVENT_ID", preferredEventsList.get(position).getEventID());
        startActivity(detailIntent);
    }

    public static String[] convertStringToArray(String str) {
        return str.split(AConstants.STR_SEPARATOR);
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }
}
