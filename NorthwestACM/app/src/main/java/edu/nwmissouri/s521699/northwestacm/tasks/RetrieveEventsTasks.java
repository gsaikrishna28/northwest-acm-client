package edu.nwmissouri.s521699.northwestacm.tasks;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import edu.nwmissouri.s521699.northwestacm.database.EventsDBAdapter;
import edu.nwmissouri.s521699.northwestacm.receivers.EventBR;
import edu.nwmissouri.s521699.northwestacm.utils.AConstants;

public class RetrieveEventsTasks extends AsyncTask<Void, Void, Void> {

    private EventTaskCallback eventTaskCallback;
    private EventsDBAdapter eventsDBAdapter;
    private Context context;

    public void setEventTaskCallback(EventTaskCallback eventTaskCallback) {
        this.eventTaskCallback = eventTaskCallback;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... params) {

        if (eventsDBAdapter == null) {
            eventsDBAdapter = new EventsDBAdapter(context);
            eventsDBAdapter.open();
        }

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Events");
        query.orderByDescending("sortingDate");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> eventsList, ParseException e) {
                if (e == null) {
                    if (eventsList.size() > 0) {
                        boolean eventRetrieved = false;
                        for (ParseObject event : eventsList) {
                            String eventName = event.getString("eventName");
                            String eventDescription = event.getString("eventDescription");
                            String eventLocation = event.getString("eventLocation");
                            String eventDate = event.getString("eventDate");
                            String eventTime = event.getString("eventTime");
                            ArrayList<String> sigGroups = new ArrayList<String>();
                            sigGroups = (ArrayList<String>) event.get("sigGroups");

                            String[] sigGroupsStringArray = new String[sigGroups.size()];
                            sigGroupsStringArray = sigGroups.toArray(sigGroupsStringArray);

                            String sigGroup = convertArrayToString(sigGroupsStringArray);

                            if (eventsDBAdapter.isEventStored(eventName, eventDescription, eventLocation, eventDate, eventTime, sigGroup)) {
                                eventRetrieved = true;
                            }

                            if (!eventRetrieved) {

                                eventsDBAdapter.insertEvent(eventName, eventDescription, eventLocation, eventDate, eventTime, sigGroup);
                                eventTaskCallback.onRetrievedNewEvent();
                                Calendar cal = Calendar.getInstance(TimeZone.getDefault());

                                int hour = Integer.parseInt(eventTime.split(":")[0]);
                                int minute = Integer.parseInt((eventTime.split(":"))[1].split(" ")[0]);
                                int month = Integer.parseInt(eventDate.split("/")[0]);
                                int day = Integer.parseInt(eventDate.split("/")[1]);
                                int year = Integer.parseInt(eventDate.split("/")[2]);

                                cal.clear();
                                cal.set(year, month, day);

                                if (eventTime.split(" ")[1].equals("AM")) {
                                    cal.set(Calendar.AM_PM, 0);
                                } else {
                                    cal.set(Calendar.AM_PM, 1);
                                }

                                cal.set(Calendar.HOUR, hour);
                                cal.set(Calendar.MINUTE, minute);

                                Intent intent = new Intent(context, EventBR.class);

                                intent.putExtra(AConstants.EVENT_NAME, eventName);
                                intent.putExtra(AConstants.EVENT_TIME, eventTime);
                                intent.putExtra(AConstants.EVENT_LOCATION, eventLocation);

                                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, eventName.hashCode() +
                                        eventLocation.hashCode() + eventTime.hashCode() + eventDate.hashCode(), intent, 0);

                                AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

                                // Registering an event to notify user at that event time
                                alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis() - 3600000, pendingIntent);
                            }

                            eventRetrieved = false;
                        }
                    }
                } else {
                    Toast.makeText(context, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        eventTaskCallback.onRetrievedNewEvent();
        eventTaskCallback.onFinishRetrievingEvents();
    }

    public interface EventTaskCallback {
        void onRetrievedNewEvent();
        void onFinishRetrievingEvents();
    }

    public static String convertArrayToString(String[] array) {
        String str = "";
        for (int i = 0; i < array.length; i++) {
            str = str + array[i];
            if (i < array.length - 1) {
                str = str + AConstants.STR_SEPARATOR;
            }
        }
        return str;
    }
}
