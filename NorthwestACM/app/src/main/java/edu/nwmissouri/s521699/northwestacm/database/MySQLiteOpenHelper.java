package edu.nwmissouri.s521699.northwestacm.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import edu.nwmissouri.s521699.northwestacm.utils.AConstants;

// This class is used to create Events table
public class MySQLiteOpenHelper extends SQLiteOpenHelper {

    static final String CREATE_TABLE_STATEMENT =
            "create table " + AConstants.EVENT_TABLE_NAME + " ( " + AConstants.EVENT_COL_ID + " integer primary key autoincrement, " + AConstants.EVENT_COL_NAME + " text not null, " + AConstants.EVENT_COL_DESCRIPTION + " text not null," + AConstants.EVENT_COL_LOCATION+ " text not null, " + AConstants.EVENT_COL_DATE + " text not null," + AConstants.EVENT_COL_TIME + " text not null, " + AConstants.EVENT_COL_SIG_GROUPS + " text not null);";

    public MySQLiteOpenHelper(Context context) {
        super(context, AConstants.DATABASE_NAME, null, AConstants.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_STATEMENT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}

