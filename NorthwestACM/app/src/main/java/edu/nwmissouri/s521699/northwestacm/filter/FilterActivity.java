package edu.nwmissouri.s521699.northwestacm.filter;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import edu.nwmissouri.s521699.northwestacm.MainActivity;
import edu.nwmissouri.s521699.northwestacm.R;
import edu.nwmissouri.s521699.northwestacm.utils.AConstants;

public class FilterActivity extends AppCompatActivity {

    private CheckBox filter_programmingContestsCB;
    private CheckBox filter_webDesignCB;
    private CheckBox filter_openSourceCB;
    private CheckBox filter_gameDevelopmentCB;
    private CheckBox filter_dataScienceCB;
    private CheckBox filter_androidCB;
    private CheckBox filter_otherCB;
    private MenuItem doneMenuItem;
    private SharedPreferences sharedPreferences;

    private boolean isProgrammingContestsChecked;
    private boolean isWebDesignChecked;
    private boolean isOpenSourceChecked;
    private boolean isGameDevelopmentChecked;
    private boolean isDataScienceChecked;
    private boolean iSAndroidChecked;
    private boolean iSOtherChecked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_menu);

        // Getting action bar
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        assert getSupportActionBar() != null;
        // Enabling action bar home up button
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.clear_24);
        actionBar.setTitle("");

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        filter_programmingContestsCB = (CheckBox) findViewById(R.id.filter_programmingContestsCB);
        filter_webDesignCB = (CheckBox) findViewById(R.id.filter_webDesignCB);
        filter_openSourceCB = (CheckBox) findViewById(R.id.filter_openSourceCB);
        filter_gameDevelopmentCB = (CheckBox) findViewById(R.id.filter_gameDevelopmentCB);
        filter_dataScienceCB = (CheckBox) findViewById(R.id.filter_dataScienceCB);
        filter_androidCB = (CheckBox) findViewById(R.id.filter_androidCB);
        filter_otherCB = (CheckBox) findViewById(R.id.filter_otherCB);

        filter_programmingContestsCB.setChecked(sharedPreferences.getBoolean(AConstants.IS_PROGRAMMINGCONTESTS_CHECKED, true));
        filter_webDesignCB.setChecked(sharedPreferences.getBoolean(AConstants.IS_WEBDESIGN_CHECKED, true));
        filter_openSourceCB.setChecked(sharedPreferences.getBoolean(AConstants.IS_OPENSOURCE_CHECKED, true));
        filter_gameDevelopmentCB.setChecked(sharedPreferences.getBoolean(AConstants.IS_GAMEDEVELOPMENT_CHECKED, true));
        filter_dataScienceCB.setChecked(sharedPreferences.getBoolean(AConstants.IS_DATASCIENCE_CHECKED, true));
        filter_androidCB.setChecked(sharedPreferences.getBoolean(AConstants.IS_ANDROID_CHECKED, true));
        filter_otherCB.setChecked(sharedPreferences.getBoolean(AConstants.IS_OTHER_CHECKED, true));

        isProgrammingContestsChecked = sharedPreferences.getBoolean(AConstants.IS_PROGRAMMINGCONTESTS_CHECKED, true);
        isWebDesignChecked = sharedPreferences.getBoolean(AConstants.IS_WEBDESIGN_CHECKED, true);
        isOpenSourceChecked = sharedPreferences.getBoolean(AConstants.IS_OPENSOURCE_CHECKED, true);
        isGameDevelopmentChecked = sharedPreferences.getBoolean(AConstants.IS_GAMEDEVELOPMENT_CHECKED, true);
        isDataScienceChecked = sharedPreferences.getBoolean(AConstants.IS_DATASCIENCE_CHECKED, true);
        iSAndroidChecked = sharedPreferences.getBoolean(AConstants.IS_ANDROID_CHECKED, true);
        iSOtherChecked = sharedPreferences.getBoolean(AConstants.IS_OTHER_CHECKED, true);

        filter_programmingContestsCB.setOnCheckedChangeListener(onCheckedChangeListener);
        filter_webDesignCB.setOnCheckedChangeListener(onCheckedChangeListener);
        filter_openSourceCB.setOnCheckedChangeListener(onCheckedChangeListener);
        filter_gameDevelopmentCB.setOnCheckedChangeListener(onCheckedChangeListener);
        filter_dataScienceCB.setOnCheckedChangeListener(onCheckedChangeListener);
        filter_androidCB.setOnCheckedChangeListener(onCheckedChangeListener);
        filter_otherCB.setOnCheckedChangeListener(onCheckedChangeListener);

    }

    CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            SharedPreferences.Editor edit = sharedPreferences.edit();

            if (!doneMenuItem.isVisible()) {
                doneMenuItem.setVisible(true);
            }

            if (isChecked) {
                if (buttonView == filter_programmingContestsCB) {
                    edit.putBoolean(AConstants.IS_PROGRAMMINGCONTESTS_CHECKED, true);
                } else if (buttonView == filter_webDesignCB) {
                    edit.putBoolean(AConstants.IS_WEBDESIGN_CHECKED, true);
                } else if (buttonView == filter_openSourceCB) {
                    edit.putBoolean(AConstants.IS_OPENSOURCE_CHECKED, true);
                } else if (buttonView == filter_gameDevelopmentCB) {
                    edit.putBoolean(AConstants.IS_GAMEDEVELOPMENT_CHECKED, true);
                } else if (buttonView == filter_dataScienceCB) {
                    edit.putBoolean(AConstants.IS_DATASCIENCE_CHECKED, true);
                } else if (buttonView == filter_androidCB) {
                    edit.putBoolean(AConstants.IS_ANDROID_CHECKED, true);
                }else if (buttonView == filter_otherCB) {
                    edit.putBoolean(AConstants.IS_OTHER_CHECKED, true);
                }
            } else {
                if (buttonView == filter_programmingContestsCB) {
                    edit.putBoolean(AConstants.IS_PROGRAMMINGCONTESTS_CHECKED, false);
                } else if (buttonView == filter_webDesignCB) {
                    edit.putBoolean(AConstants.IS_WEBDESIGN_CHECKED, false);
                } else if (buttonView == filter_openSourceCB) {
                    edit.putBoolean(AConstants.IS_OPENSOURCE_CHECKED, false);
                } else if (buttonView == filter_gameDevelopmentCB) {
                    edit.putBoolean(AConstants.IS_GAMEDEVELOPMENT_CHECKED, false);
                } else if (buttonView == filter_dataScienceCB) {
                    edit.putBoolean(AConstants.IS_DATASCIENCE_CHECKED, false);
                } else if (buttonView == filter_androidCB) {
                    edit.putBoolean(AConstants.IS_ANDROID_CHECKED, false);
                }else if (buttonView == filter_otherCB) {
                    edit.putBoolean(AConstants.IS_OTHER_CHECKED, false);
                }
            }

            edit.apply();
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.filter_menu, menu);

        doneMenuItem = menu.findItem(R.id.done_action);
        doneMenuItem.setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.done_action:

                if (!filter_programmingContestsCB.isChecked() && !filter_webDesignCB.isChecked() &&
                        !filter_openSourceCB.isChecked() && !filter_gameDevelopmentCB.isChecked() &&
                        !filter_dataScienceCB.isChecked() && !filter_androidCB.isChecked() &&
                        !filter_otherCB.isChecked()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(FilterActivity.this);
                    builder.setMessage("Select at least one group.");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.show();
                } else {
                    MainActivity.isFilterActivityDismissed = true;
                    finish();
                }

                break;
            case android.R.id.home:
                SharedPreferences.Editor edit = sharedPreferences.edit();

                edit.putBoolean(AConstants.IS_PROGRAMMINGCONTESTS_CHECKED, isProgrammingContestsChecked);
                edit.putBoolean(AConstants.IS_WEBDESIGN_CHECKED, isWebDesignChecked);
                edit.putBoolean(AConstants.IS_OPENSOURCE_CHECKED, isOpenSourceChecked);
                edit.putBoolean(AConstants.IS_GAMEDEVELOPMENT_CHECKED, isGameDevelopmentChecked);
                edit.putBoolean(AConstants.IS_DATASCIENCE_CHECKED, isDataScienceChecked);
                edit.putBoolean(AConstants.IS_ANDROID_CHECKED, iSAndroidChecked);
                edit.putBoolean(AConstants.IS_OTHER_CHECKED, iSOtherChecked);

                edit.apply();
                finish();
                break;
        }

        return true;
    }

}
