package edu.nwmissouri.s521699.northwestacm;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;

import com.github.paolorotolo.appintro.AppIntro;

import edu.nwmissouri.s521699.northwestacm.utils.AConstants;
import layout.Screen1;
import layout.Screen2;
import layout.Screen3;
import layout.Screen4;
import layout.Screen5;
import layout.Screen6;

public class TutorialActivity extends AppIntro {

    private SharedPreferences sharedPreferences;

    @Override
    public void init(Bundle bundle) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryDark));
        }

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        if (sharedPreferences.getBoolean(AConstants.IS_TUORIAL_VIEWED, false)) {
            loadMainActivity();
        } else {

            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putBoolean(AConstants.IS_TUORIAL_VIEWED, true);
            edit.apply();

            addSlide(new Screen1());
            addSlide(new Screen2());
            addSlide(new Screen3());
            addSlide(new Screen4());
            addSlide(new Screen5());
            addSlide(new Screen6());

            setBarColor(Color.parseColor("#0063B1"));
            setSeparatorColor(Color.parseColor("#0063B1"));

            // Show Skip/Done button
            showSkipButton(true);
            showDoneButton(true);

            // Turn vibration on and set intensity
            setVibrate(true);
            setVibrateIntensity(30);

        }
    }

    private void loadMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        finish();
        startActivity(intent);

    }

    @Override
    public void onSkipPressed() {
        loadMainActivity();
       // Toast.makeText(getApplicationContext(), "Tutorial skipped", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNextPressed() {

    }

    @Override
    public void onDonePressed() {
        loadMainActivity();
    }

    @Override
    public void onSlideChanged() {

    }
}
