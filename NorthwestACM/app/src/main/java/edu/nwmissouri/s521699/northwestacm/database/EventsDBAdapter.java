package edu.nwmissouri.s521699.northwestacm.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;


import edu.nwmissouri.s521699.northwestacm.utils.AConstants;

public class EventsDBAdapter {

    private Context context;
    private MySQLiteOpenHelper mySQLiteOpenHelper;
    private SQLiteDatabase eventsDB;

    public EventsDBAdapter(Context context) {
        this.context = context;
    }

    public EventsDBAdapter open() {
        mySQLiteOpenHelper = new MySQLiteOpenHelper(context);
        eventsDB = mySQLiteOpenHelper.getWritableDatabase();
        return EventsDBAdapter.this;
    }

    public void close() {
        mySQLiteOpenHelper.close();
    }

    // Inserts a record of event in Events table
    public long insertEvent(String eventName, String eventDescription, String eventLocation, String eventDate, String eventTime, String sigGroups) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(AConstants.EVENT_COL_NAME, eventName);
        contentValues.put(AConstants.EVENT_COL_DESCRIPTION, eventDescription);
        contentValues.put(AConstants.EVENT_COL_LOCATION, eventLocation);
        contentValues.put(AConstants.EVENT_COL_DATE, eventDate);
        contentValues.put(AConstants.EVENT_COL_TIME, eventTime);
        contentValues.put(AConstants.EVENT_COL_SIG_GROUPS, sigGroups);

        return eventsDB.insert(AConstants.EVENT_TABLE_NAME, null, contentValues);
    }

    // Returns all events
    public Cursor getAllEvents() {
        return eventsDB.query(AConstants.EVENT_TABLE_NAME, new String[]{AConstants.EVENT_COL_ID, AConstants.EVENT_COL_NAME,
                        AConstants.EVENT_COL_DESCRIPTION, AConstants.EVENT_COL_LOCATION, AConstants.EVENT_COL_DATE,
                        AConstants.EVENT_COL_TIME, AConstants.EVENT_COL_SIG_GROUPS}, null, null, null, null,
                AConstants.EVENT_COL_DATE + " DESC, " + AConstants.EVENT_COL_TIME + " DESC");
    }

    // Checks if event is already stored or not
    public boolean isEventStored(String eventName, String eventDescription, String eventLocation, String eventDate, String eventTime, String sigGroups) {

        Cursor cursor = eventsDB.query(true, AConstants.EVENT_TABLE_NAME, new String[]{AConstants.EVENT_COL_NAME,
                        AConstants.EVENT_COL_DESCRIPTION, AConstants.EVENT_COL_LOCATION, AConstants.EVENT_COL_DATE,
                        AConstants.EVENT_COL_TIME, AConstants.EVENT_COL_SIG_GROUPS},
                AConstants.EVENT_COL_NAME + " = ? AND " + AConstants.EVENT_COL_DESCRIPTION + " = ? AND "
                        + AConstants.EVENT_COL_LOCATION + " = ? AND " + AConstants.EVENT_COL_DATE + " = ? AND "
                        + AConstants.EVENT_COL_TIME + " = ? AND " + AConstants.EVENT_COL_SIG_GROUPS + " = ?", new String[]{eventName, eventDescription, eventLocation, eventDate, eventTime, sigGroups}, null, null, null, null);

        if (cursor.getCount() == 0) {
            return false;
        } else {
            return true;
        }
    }

    // Returns an event based on event ID
    public Cursor getEvent(long eventId) throws SQLException {
        Cursor mCursor =
                eventsDB.query(true, AConstants.EVENT_TABLE_NAME, new String[]{AConstants.EVENT_COL_ID, AConstants.EVENT_COL_NAME,
                                AConstants.EVENT_COL_DESCRIPTION, AConstants.EVENT_COL_LOCATION, AConstants.EVENT_COL_DATE, AConstants.EVENT_COL_TIME, AConstants.EVENT_COL_SIG_GROUPS}, AConstants.EVENT_COL_ID + " = " + eventId, null,
                        null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    // Returns an event based on event name, event location, event time
    public Cursor getEvent(String eventName, String eventLocation, String eventTime) throws SQLException {

        Cursor mCursor =
                eventsDB.query(true, AConstants.EVENT_TABLE_NAME, new String[]{AConstants.EVENT_COL_ID, AConstants.EVENT_COL_NAME,
                                AConstants.EVENT_COL_DESCRIPTION, AConstants.EVENT_COL_LOCATION
                                , AConstants.EVENT_COL_DATE, AConstants.EVENT_COL_TIME,
                                AConstants.EVENT_COL_SIG_GROUPS},
                        AConstants.EVENT_COL_NAME + " = ? AND " + AConstants.EVENT_COL_LOCATION + " = ? AND " +
                                AConstants.EVENT_COL_TIME + " = ?"
                        , new String[]{eventName, eventLocation, eventTime},
                        null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    // Deletes an event based on event id
    public void delete(int id) {
        eventsDB.delete(AConstants.EVENT_TABLE_NAME, AConstants.EVENT_COL_ID + "=" + id, null);
    }

    // Deletes an event based on event name, event location, event time, event date and sig's
    public void delete(String eventName, String eventDescription, String eventLocation, String eventTime, String eventDate, String sigGroups) {
        eventsDB.delete(AConstants.EVENT_TABLE_NAME, AConstants.EVENT_COL_NAME + " = ? AND " +
                        AConstants.EVENT_COL_DESCRIPTION + " = ? AND " + AConstants.EVENT_COL_LOCATION + " = ? AND " +
                        AConstants.EVENT_COL_DATE + " = ? AND " + AConstants.EVENT_TIME + " = ? AND " +
                        AConstants.EVENT_COL_SIG_GROUPS + " = ?",
                new String[]{eventName, eventDescription, eventLocation, eventDate, eventTime, sigGroups});
    }
}
