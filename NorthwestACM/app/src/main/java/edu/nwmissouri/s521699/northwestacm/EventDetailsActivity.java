package edu.nwmissouri.s521699.northwestacm;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

import edu.nwmissouri.s521699.northwestacm.database.EventsDBAdapter;
import edu.nwmissouri.s521699.northwestacm.events.EventsList;
import edu.nwmissouri.s521699.northwestacm.utils.AConstants;

public class EventDetailsActivity extends AppCompatActivity {

    private EventsList.Event event;
    EventsDBAdapter eventsDBAdapter;
    Cursor eventsCursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);

        ActionBar actionBar = getSupportActionBar();
        assert getSupportActionBar() != null;
        actionBar.setDisplayHomeAsUpEnabled(true);

        TextView eventNameTV = (TextView) findViewById(R.id.detail_eventNameTV);
        TextView eventDescriptionTV = (TextView) findViewById(R.id.detail_eventDescriptionTV);
        TextView eventLocationTV = (TextView) findViewById(R.id.detail_eventLocationTV);
        TextView eventDateTV = (TextView) findViewById(R.id.detail_eventDateTV);
        TextView eventTimeTV = (TextView) findViewById(R.id.detail_eventTimeTV);

        CheckBox programmingContestCB = (CheckBox) findViewById(R.id.detail_programmingContestsCB);
        CheckBox webDesignCB = (CheckBox) findViewById(R.id.detail_webDesignCB);
        CheckBox openSourceCB = (CheckBox) findViewById(R.id.detail_openSourceCB);
        CheckBox gameDevelopmentCB = (CheckBox) findViewById(R.id.detail_gameDevelopmentCB);
        CheckBox dataScienceCB = (CheckBox) findViewById(R.id.detail_dataScienceCB);
        CheckBox androidCB = (CheckBox) findViewById(R.id.detail_androidCB);
        CheckBox otherCB = (CheckBox) findViewById(R.id.detail_otherCB);

        int eventID;
        String eventName;
        String eventDescription;
        String eventLocation;
        String eventDate;
        String eventTime;

        if (getIntent().getBooleanExtra(AConstants.IS_NOTIFIED, false)) {

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(10);

            eventName = getIntent().getStringExtra(AConstants.EVENT_NAME);
            eventLocation = getIntent().getStringExtra(AConstants.EVENT_LOCATION);
            eventTime = getIntent().getStringExtra(AConstants.EVENT_TIME);

            eventsDBAdapter = new EventsDBAdapter(EventDetailsActivity.this);
            eventsDBAdapter.open();

            eventsCursor = eventsDBAdapter.getEvent(eventName, eventLocation, eventTime);
            eventsCursor.moveToFirst();

            if (eventsCursor != null && eventsCursor.getCount() > 0) {
                do {
                    eventID = eventsCursor.getInt(eventsCursor.getColumnIndex(AConstants.EVENT_COL_ID));
                    eventName = eventsCursor.getString(eventsCursor.getColumnIndex(AConstants.EVENT_COL_NAME));
                    eventDescription = eventsCursor.getString(eventsCursor.getColumnIndex(AConstants.EVENT_COL_DESCRIPTION));
                    eventLocation = eventsCursor.getString(eventsCursor.getColumnIndex(AConstants.EVENT_COL_LOCATION));
                    eventDate = eventsCursor.getString(eventsCursor.getColumnIndex(AConstants.EVENT_COL_DATE));
                    eventTime = eventsCursor.getString(eventsCursor.getColumnIndex(AConstants.EVENT_COL_TIME));
                    ArrayList<String> sigGroupsList = new ArrayList<>(Arrays.asList(convertStringToArray(eventsCursor.getString(eventsCursor.getColumnIndex(AConstants.EVENT_COL_SIG_GROUPS)))));
                    event = new EventsList.Event(eventID, eventName, eventDescription, eventLocation, eventDate, eventTime, sigGroupsList);
                } while (eventsCursor.moveToNext());
            }

        } else {

            eventID = getIntent().getIntExtra("EVENT_ID", 0);

            eventsDBAdapter = new EventsDBAdapter(EventDetailsActivity.this);
            eventsDBAdapter.open();

            eventsCursor = eventsDBAdapter.getEvent(eventID);
            eventsCursor.moveToFirst();

            if (eventsCursor != null && eventsCursor.getCount() > 0) {
                do {
                    eventName = eventsCursor.getString(eventsCursor.getColumnIndex(AConstants.EVENT_COL_NAME));
                    eventDescription = eventsCursor.getString(eventsCursor.getColumnIndex(AConstants.EVENT_COL_DESCRIPTION));
                    eventLocation = eventsCursor.getString(eventsCursor.getColumnIndex(AConstants.EVENT_COL_LOCATION));
                    eventDate = eventsCursor.getString(eventsCursor.getColumnIndex(AConstants.EVENT_COL_DATE));
                    eventTime = eventsCursor.getString(eventsCursor.getColumnIndex(AConstants.EVENT_COL_TIME));
                    ArrayList<String> sigGroupsList = new ArrayList<>(Arrays.asList(convertStringToArray(eventsCursor.getString(eventsCursor.getColumnIndex(AConstants.EVENT_COL_SIG_GROUPS)))));
                    event = new EventsList.Event(eventID, eventName, eventDescription, eventLocation, eventDate, eventTime, sigGroupsList);

                } while (eventsCursor.moveToNext());
            }

        }

        if (event != null) {

            eventNameTV.setText(event.getEventName());
            eventDescriptionTV.setText(event.getEventDescription());
            eventLocationTV.setText(event.getEventLocation());
            eventDateTV.setText(event.getEventDate());
            eventTimeTV.setText(event.getEventTime());

            for (String sigGroup : event.getSigGroups()) {
                switch (sigGroup) {
                    case "P":
                        programmingContestCB.toggle();
                        break;
                    case "W":
                        webDesignCB.toggle();
                        break;
                    case "O":
                        openSourceCB.toggle();
                        break;
                    case "G":
                        gameDevelopmentCB.toggle();
                        break;
                    case "D":
                        dataScienceCB.toggle();
                        break;
                    case "A":
                        androidCB.toggle();
                        break;
                    case "T":
                        otherCB.toggle();
                        break;
                }
            }
        } else {
            if (EventsList.eventsList.contains(event)) {

            } else {
                Toast.makeText(EventDetailsActivity.this, "Sorry, This event has been cancelled.", Toast.LENGTH_SHORT).show();
                finish();
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    public static String[] convertStringToArray(String str) {
        return str.split(AConstants.STR_SEPARATOR);
    }
}
