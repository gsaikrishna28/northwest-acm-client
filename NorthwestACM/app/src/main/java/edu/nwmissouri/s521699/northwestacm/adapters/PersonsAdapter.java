package edu.nwmissouri.s521699.northwestacm.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import edu.nwmissouri.s521699.northwestacm.R;
import edu.nwmissouri.s521699.northwestacm.persons.PersonsList;

public class PersonsAdapter extends ArrayAdapter<PersonsList.Person> {

    public PersonsAdapter(Context context, int resource, int textViewResourceId, List<PersonsList.Person> objects) {
        super(context, resource, textViewResourceId, objects);
    }

    // Returns the view of contact list item
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view =  super.getView(position, convertView, parent);

        // Getting the resources of text views
        TextView personNameTV = (TextView)view.findViewById(R.id.listItemPersonNameTV);
        TextView personRoleTV = (TextView)view.findViewById(R.id.listItemPersonRoleTV);
        TextView personEmailTV = (TextView)view.findViewById(R.id.listItemPersonEmailTV);
        TextView personPhoneNumTV = (TextView)view.findViewById(R.id.listItemPersonPhoneNumberTV);

        // Setting the text view's text
        personNameTV.setText(getItem(position).getPersonName());
        personRoleTV.setText(String.format("- %s", getItem(position).getPersonRole()));
        personEmailTV.setText(getItem(position).getPersonEmail());
        personPhoneNumTV.setText(getItem(position).getPersonPhoneNum());

        return view;
    }

}
